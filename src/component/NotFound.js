import React from "react";
import { NavLink } from "react-router-dom";

const NotFound = () => {
  return (
    <div className="NotFound">
      <div className="NotFoundContent">
        <h3>Cette page n'existe pas !</h3>
        <NavLink exact to="/">
          <i class="fa-solid fa-house"></i>
          <span>Accueil</span>
        </NavLink>
      </div>
    </div>
  );
};

export default NotFound;
