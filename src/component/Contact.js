import React from "react";
import Navigation from "./Navigation";
// import CopyToClipboard from "react-copy-to-clipboard";

const Contact = () => {
  return (
    <div className="contact">
      <Navigation />
      <div className="contactContent">
        <div className="header"></div>
        <div className="contactBox">
          <h1>Contactez-moi</h1>
          <ul>
            <li>
              <i className="fa-solid fa-location-pin"></i>
              <span>Corbeil-Essonnes</span>
            </li>
            <li>
              <i className="fa-sharp fa-solid fa-mobile-retro"></i>
              <span className="clickInput">06 62 45 98 04</span>
            </li>
            <li>
              <i class="fa-sharp fa-solid fa-envelope"></i>
              <span className="clickInput">guinier95.js@gmail.com</span>
            </li>
          </ul>
        </div>
        <div className="socialNetwork">
          <ul>
            <li>
              <a
                href="https://www.linkedin.com/in/junior-guinier-820b30241/"
                target={"_blank "}
              >
                <i class="fa-brands fa-linkedin"></i>
              </a>
            </li>
            <li>
              <a href="https://github.com/BaracusHanson" target={"_blank"}>
                <i className="fa-brands fa-github"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Contact;
