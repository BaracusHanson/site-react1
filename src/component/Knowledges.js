import React from "react";
import Experience from "./kowledges/Experience";
import Hobbies from "./kowledges/Hobbies";
import Languages from "./kowledges/Languages";
import OthersSkills from "./kowledges/OthersSkills";
import Navigation from "./Navigation";

const Knowledges = () => {
  return (
    <div className="knowledges">
      <Navigation />
      <div className="knowledgesContent">
        <Languages />
        <Experience />
        <OthersSkills />
        <Hobbies />
      </div>
    </div>
  );
};

export default Knowledges;
