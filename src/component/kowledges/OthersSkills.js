import React from "react";

const OthersSkills = () => {
  return (
    <div className="otherSkills">
      <h3>Autres compétences</h3>
      <div className="list">
        <ul>
          <li>
            <i class="fa-sharp fa-solid fa-circle-check"></i> Anglais C1
          </li>
          <li>
            <i class="fa-sharp fa-solid fa-circle-check"></i> GitHub
          </li>
          <li>
            <i class="fa-sharp fa-solid fa-circle-check"></i> Figma
          </li>
          <li>
            <i class="fa-sharp fa-solid fa-circle-check"></i> Word 2019
          </li>
          <li>
            <i class="fa-sharp fa-solid fa-circle-check"></i> Exel 2019
          </li>
        </ul>
      </div>
    </div>
  );
};

export default OthersSkills;
