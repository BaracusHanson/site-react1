import React, { Component } from "react";
import ProgressBar from "./ProgressBar";

class languages extends Component {
  state = {
    languages: [
      { id: 1, value: "Javascript", xp: 4 },
      { id: 2, value: "Css", xp: 4 },
      { id: 3, value: "Php", xp: 0.2 },
      { id: 4, value: "Python", xp: 4 },
    ],

    frameworks: [
      { id: 1, value: "React", xp: 0.2 },
      { id: 2, value: "Bootstrap", xp: 4 },
      { id: 3, value: "Sass", xp: 0.4 },
      { id: 4, value: "Material UI", xp: 0.3 },
    ],
  };
  render() {
    let { languages, frameworks } = this.state;
    return (
      <div className="languagesFrameworks">
        <ProgressBar
          languages={languages}
          className="languagesDisplay"
          title="Languages"
        />
        <ProgressBar
          languages={frameworks}
          className="FrameworksDisplay"
          title="Frameworks"
        />
      </div>
    );
  }
}

export default languages;
