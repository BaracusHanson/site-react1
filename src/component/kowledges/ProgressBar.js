import React from "react";

const ProgressBar = (props) => {
  return (
    <div className={props.className}>
      <h3>{props.title}</h3>
      <div className="years">
        <span>Mois d'exprériences</span>
        <span>12 mois</span>
      </div>
      {props.languages.map((item) => {
        let xpYears = 12;
        let prograssBar = (item.xp / xpYears) * 100 + "%";
        return (
          <div key={item.id} className="languagesList">
            <li>{item.value}</li>
            <div className="progressBar" style={{ width: prograssBar }}></div>
          </div>
        );
      })}
    </div>
  );
};

export default ProgressBar;
