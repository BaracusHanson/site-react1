import React from "react";

const Hobbies = () => {
  return (
    <div className="hobbies">
      <h3>Centre d'interet</h3>
      <ul className="hobby">
        <li>
          <i class="fa-sharp fa-solid fa-barcode"></i>
          <span>Codage</span>
        </li>
        <li>
          <i class="fa-solid fa-music"></i>
          <span>Musique</span>
        </li>
        <li>
          <i class="fa-solid fa-person-running"></i>
          <span>Sport</span>
        </li>
        <li>
          <i class="fa-solid fa-rocket"></i>
          <span>Astronomie</span>
        </li>
        <li>
          <i class="fa-solid fa-pen-ruler"></i>
          <span>UE5</span>
        </li>
      </ul>
    </div>
  );
};

export default Hobbies;
