import React from "react";
import Navigation from "./Navigation";
import ProjectList from "./portfolio/ProjectList";
import { NavLink } from "react-router-dom";

const Portfolio = () => {
  return (
    <div className="portfolio">
      {/* <Navigation /> */}
      {/* <ProjectList /> */}
      <div className="NotFoundContent">
        <h3>En cours de réalisation...</h3>
        <NavLink exact to="/">
          <i class="fa-solid fa-house"></i>
          <span>Accueil</span>
        </NavLink>
      </div>
    </div>
  );
};

export default Portfolio;
