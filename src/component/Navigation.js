import React from "react";
import { NavLink } from "react-router-dom";

const Navigation = () => {
  return (
    <div className="sidebar">
      <div className="id">
        <div className="idContent">
          <img src="./media/photocv.jpeg" alt="pic de profil" />
          <h3>Joseph J Guinier</h3>
        </div>
      </div>
      <div className="navigation">
        <ul>
          <li>
            <NavLink exact to="/" activeClassName="navActive">
              <i class="fa-solid fa-house"></i>
              <span>Accueil</span>
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/Knowledges" activeClassName="navActive">
              <i class="fa-solid fa-mountain"></i>
              <span>Compétences</span>
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/Portfolio" activeClassName="navActive">
              <i class="fa-solid fa-image"></i>
              <span>Portefolio</span>
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/Contact" activeClassName="navActive">
              <i class="fa-solid fa-phone"></i>
              <span>Contact</span>
            </NavLink>
          </li>
        </ul>
      </div>
      <div className="socialNetwork">
        <ul>
          <li>
            <a
              href="https://www.linkedin.com/in/junior-guinier-820b30241/"
              target={"_blank "}
            >
              <i class="fa-brands fa-linkedin"></i>
            </a>
          </li>
          <li>
            <a href="https://github.com/BaracusHanson" target={"_blank"}>
              <i className="fa-brands fa-github"></i>
            </a>
          </li>
        </ul>
      </div>
      <div className="signature">
        <p>Joseph J Guinier-2022</p>
      </div>
    </div>
  );
};

export default Navigation;
