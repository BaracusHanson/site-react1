import React from "react";
import Navigation from "./Navigation";

const Home = () => {
  return (
    <div className="Home">
      <Navigation />
      <div className="homeContent">
        <div className="content">
          <h1>Joseph Guinier</h1>
          <h2>Concepteur developpeur mobile</h2>
          <div className="pdf">
            <a href="./media/cv-2022.pdf" target={"_blank"}>
              Télécharger CV
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
